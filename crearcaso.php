
<?php

	function multiexplode ($delimiters,$string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}

	$username=$_GET['username'];

	$soap_request = '<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
	<soapenv:Header/>
		<soapenv:Body>
		<tem:createCasesAsString>
		<!--Optional:-->
			<tem:casesInfo>
			<![CDATA[<BizAgiWSParam>
				<domain>CEA</domain>
				<userName>'.$username.'</userName>
					<Cases>
					<Case>
					<Process>PadronDesarrolladores</Process>
					</Case>
					</Cases>
			</BizAgiWSParam>]]>
			</tem:casesInfo>
		</tem:createCasesAsString>
		</soapenv:Body>
	</soapenv:Envelope>';

    $headers = array(
	"Content-type: text/xml",
	"Accept: text/xml",
	"Cache-Control: no-cache",
	"Pragma: no-cache",
	"SOAPAction: http://tempuri.org/createCasesAsString",
	"Content-length: ".strlen($soap_request),
    );

    $url = "http://10.1.1.155/scg/WebServices/WorkflowEngineSOA.asmx";

    $ch = curl_init($url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_request);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, true);
	curl_setopt($ch, CURLOPT_TIMEOUT,10);
     
    $output = curl_exec($ch);
    curl_close($ch);
    $datos = multiexplode(array("&lt;","/","&gt;"), $output);
    $radNumber=$datos['29'];
    //echo "<h1>".$radNumber."</h1>";
	
/////////////////////////////////////////////////////ENVIAR A LA CEA///////////////////////////////////////////////////////////////////////////////////////
$RFC=$_POST['RFC2'];
$username2=$_GET['username'];
$Tipomovimiento=$_POST['TipoMovimiento2'];
$Personamoral=$_POST['PersonaMoral2'];
$Nombre=$_POST['Nombre2'];
$Acronimo=$_POST['Acronimo2'];
$Noescritura=$_POST['Notaria2'];
$Notaria=$_POST['Notaria2'];
$Constitucion=$_POST['Constitucion2'];
/*$Fechaconstitucion=$_POST['Fechaconstitucion2'];
if($Fechaconstitucion!=""){
$Fechaconstitucion2=date_format($Fechaconstitucion,'Y-m-d'); 
}else{
$Fechaconstitucion2='';
}
$Fechavigencia=$_POST['Fechavigencia2'];
if($Fechavigencia!=""){
$Fechavigencia2=date_format($Fechavigencia,'Y-m-d'); 
}else{
$Fechavigencia2='';
}*/

$Status=$_POST['Status2'];
$CalleFiscal=$_POST['CalleFiscal2'];
$Noexteriorfiscal=$_POST['NoexteriorFiscal2'];
$Nointeriorfiscal=$_POST['NointeriorFiscal2'];
$Coloniafiscal=$_POST['ColoniaFiscal2'];
$idMunicipioFiscal=$_POST['idMunicipioFiscal2'];
$Telefonofiscal=$_POST['TelefonoFiscal2'];
$Correo=$_POST['Correo2'];
$CPFiscal=$_POST['CPFiscal2'];
$LocalidadFiscal=$_POST['LocalidadFiscal2'];
$Calle=$_POST['Calle2'];
$Noexterior=$_POST['Noexterior2'];
$Nointerior=$_POST['Nointerior2'];
$Colonia=$_POST['Colonia2'];
$Municipio=$_POST['idMunicipio2'];
$Telefono=$_POST['Telefono2'];
$CP=$_POST['CP2'];
$Localidad=$_POST['Localidad2'];
$RFCDesa=$_POST['RFCDesa2'];
$NombreRDesa=$_POST['NombreDesa2'];
$CalleDesa=$_POST['CalleDesa2'];
$NoexteriorDesa=$_POST['NoexteriorDesa2'];
$NointeriosDesa=$_POST['NointeriosDesa2'];
$ColoniaDesa=$_POST['ColoniaDesa2'];
$MunicipioDesa=$_POST['MunicipioDesa2'];
$CPDesa=$_POST['CPDesa2'];
$TelefonoDesa=$_POST['TelefonoDesa2'];
$CorreoDesa=$_POST['CorreoDesa2'];

$requiero='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
	<soapenv:Header/>
	<soapenv:Body>
	<tem:performActivityAsString>
		<!--Optional:-->
		<tem:activityInfo> 
		<![CDATA[<BizAgiWSParam>
		<domain>CEA</domain>
		<userName>'.$username2.'</userName>
		<ActivityData>
		<radNumber>'.$radNumber.'</radNumber>
		<taskId>7987</taskId>
		<taskId>7982</taskId>
		</ActivityData>
		<Entities>
			<PadronDesarrolladores>
				<RFC>'.$RFC.'</RFC>    
				<Tipomovimiento>'.$Tipomovimiento.'</Tipomovimiento>
				<Personamoral>'.$Personamoral.'</Personamoral>
				<Nombre>'.$Nombre.'</Nombre>
				<Acronimo>'.$Acronimo.'</Acronimo>
				<Noescritura>'.$Noescritura.'</Noescritura>
				<Notaria>'.$Notaria.'</Notaria>
				<Constitucion>'.$Constitucion.'</Constitucion>
				<Status>'.$Status.'</Status>
				<CalleFiscal>'.$CalleFiscal.'</CalleFiscal>
				<Noexteriorfiscal>'.$Noexteriorfiscal.'</Noexteriorfiscal>
				<Nointeriorfiscal>'.$Nointeriorfiscal.'</Nointeriorfiscal>
				<Coloniafiscal>'.$Coloniafiscal.'</Coloniafiscal>
				<MunicipioFiscal>'.$idMunicipioFiscal.'</MunicipioFiscal>
				<Telefonofiscal>'.$Telefonofiscal.'</Telefonofiscal>
				<Correo>'.$Correo.'</Correo>
				<CPFiscal>'.$CPFiscal.'</CPFiscal>
				<LocalidadFiscal>'.$LocalidadFiscal.'</LocalidadFiscal>
				<Calle>'.$Calle.'</Calle>
				<Noexterior>'.$Noexterior.'</Noexterior>
				<Nointerior>'.$Nointerior.'</Nointerior>
				<Colonia>'.$Colonia.'</Colonia>
				<Municipio>'.$Municipio.'</Municipio>
				<Telefono>'.$Telefono.'</Telefono>
				<CP>'.$CP.'</CP>
				<Localidad>'.$Localidad.'</Localidad>
				<Representantess>
					<RFC>'.$RFCDesa.'</RFC>
					<Nombre>'.$NombreRDesa.'</Nombre>
					<Calle>'.$CalleDesa.'</Calle>
					<Noexterior>'.$NoexteriorDesa.'</Noexterior>
					<Nointerios>'.$NointeriosDesa.'</Nointerios>
					<Colonia>'.$ColoniaDesa.'</Colonia>
					<Municipio>'.$MunicipioDesa.'</Municipio>
					<CP>'.$CPDesa.'</CP>
					<Telefono>'.$TelefonoDesa.'</Telefono>
					<Correo>'.$CorreoDesa.'</Correo>
				</Representantess>
			</PadronDesarrolladores>          
		</Entities>
	    </BizAgiWSParam>]]>
	</tem:activityInfo>
	</tem:performActivityAsString>
	</soapenv:Body>
	</soapenv:Envelope>';

	$headers2 = array(
	"Content-type: text/xml",
	"Accept: text/xml",
	"Cache-Control: no-cache",
	"Pragma: no-cache",
	"SOAPAction: http://tempuri.org/performActivityAsString",
	"Content-length: ".strlen($requiero),
	);

    $url2 = "http://10.1.1.155/scg/WebServices/WorkflowEngineSOA.asmx";
    $ch2 = curl_init($url2);

	curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch2, CURLOPT_POST, 1);
	curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers2);
	curl_setopt($ch2, CURLOPT_POSTFIELDS, $requiero);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch2, CURLOPT_VERBOSE, true);
	curl_setopt($ch2, CURLOPT_TIMEOUT,10);
	
	 $resultado2 = curl_exec($ch2);
?>
 <style>
	#conexion {
		width: 520px;
		height: 300px;
		background-color: #E8E7E7;
		padding: 19px 19px 10px;
	}
	body {
		padding-top: 100px;
		padding-left: 450px;
		background-color: #FFFFFF;
	}
	</style>
	<?php
	if ($resultado2) 
	{
		?>
	<body>
	<div id="conexion">
	<center>
		<h1><?php echo $radNumber; ?> </h1><br>
		<h1>Se creó la solicitud con exito</h1>
		<img src="css/correcto.png" width="100" height="100">
	</center>
	</div>
	</body>	
	<?php
	}else{
		?>
		<body>
	<div id="conexion">
	<center>
		<h1><?php echo $radNumber; ?> </h1><br>
		<h1>Hubo un error. no se pudo insertar</h1>
		<img src="css/error.png" width="100" height="100">
	</center>
	</div>
	</body>	
	<?php
	}
    curl_close($ch2);
   ?>

   
