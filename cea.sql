-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-08-2020 a las 06:33:18
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cea`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `idEstado` int(11) NOT NULL,
  `NombreEstado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`idEstado`, `NombreEstado`) VALUES
(22, 'Queretaro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `idMunicipio` int(11) NOT NULL,
  `Nombre` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`idMunicipio`, `Nombre`) VALUES
(1801, 'QUERETARO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padrondesarrolladores`
--

CREATE TABLE `padrondesarrolladores` (
  `idPadron` int(11) NOT NULL,
  `RFC` varchar(50) DEFAULT NULL,
  `Nombre` varchar(150) DEFAULT NULL,
  `Calle` varchar(150) DEFAULT NULL,
  `Noexterior` varchar(50) DEFAULT NULL,
  `Nointerior` varchar(50) DEFAULT NULL,
  `Colonia` varchar(150) DEFAULT NULL,
  `CalleFiscal` varchar(150) DEFAULT NULL,
  `NoexteriorFiscal` varchar(50) DEFAULT NULL,
  `NointeriorFiscal` varchar(50) DEFAULT NULL,
  `ColoniaFiscal` varchar(150) DEFAULT NULL,
  `idMunicipio` int(11) DEFAULT NULL,
  `idMunicipioFiscal` int(11) DEFAULT NULL,
  `PersonaMoral` bit(1) DEFAULT NULL,
  `Fechavigencia` datetime DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `CP` varchar(50) DEFAULT NULL,
  `Pais` varchar(50) DEFAULT NULL,
  `Fechaconstitucion` datetime DEFAULT NULL,
  `Notaria` varchar(50) DEFAULT NULL,
  `NoEscritura` varchar(50) DEFAULT NULL,
  `TelefonoFiscal` varchar(50) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Acronimo` varchar(150) DEFAULT NULL,
  `Localidad` varchar(150) DEFAULT NULL,
  `LocalidadFiscal` varchar(150) DEFAULT NULL,
  `CPFiscal` varchar(50) DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `Cveusr` int(11) DEFAULT NULL,
  `Cvepass` varchar(50) DEFAULT NULL,
  `AltaLog` int(11) DEFAULT NULL,
  `TipoMovimiento` int(11) DEFAULT NULL,
  `Renovacion` bit(1) DEFAULT NULL,
  `Anexos` bit(1) DEFAULT NULL,
  `idSolicituPadron` int(11) DEFAULT NULL,
  `Documento` tinyint(4) DEFAULT NULL,
  `Objeto` varchar(150) DEFAULT NULL,
  `Constitucion` int(11) DEFAULT NULL,
  `Notariaedo` int(11) DEFAULT NULL,
  `esFideicomiso` bit(1) DEFAULT NULL,
  `NombreFideicomiso` varchar(550) DEFAULT NULL,
  `Cedula` int(11) DEFAULT NULL,
  `esFideicomitente` bit(1) DEFAULT NULL,
  `esFideicomisario` bit(1) DEFAULT NULL,
  `otro` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `padrondesarrolladores`
--

INSERT INTO `padrondesarrolladores` (`idPadron`, `RFC`, `Nombre`, `Calle`, `Noexterior`, `Nointerior`, `Colonia`, `CalleFiscal`, `NoexteriorFiscal`, `NointeriorFiscal`, `ColoniaFiscal`, `idMunicipio`, `idMunicipioFiscal`, `PersonaMoral`, `Fechavigencia`, `Telefono`, `CP`, `Pais`, `Fechaconstitucion`, `Notaria`, `NoEscritura`, `TelefonoFiscal`, `Status`, `Acronimo`, `Localidad`, `LocalidadFiscal`, `CPFiscal`, `Correo`, `Cveusr`, `Cvepass`, `AltaLog`, `TipoMovimiento`, `Renovacion`, `Anexos`, `idSolicituPadron`, `Documento`, `Objeto`, `Constitucion`, `Notariaedo`, `esFideicomiso`, `NombreFideicomiso`, `Cedula`, `esFideicomitente`, `esFideicomisario`, `otro`) VALUES
(1, 'ghjgjgjgghgqw', 'pruebq', 'prueba', '123', '6', 'centro', 'prueba', '123', '6', 'prueba', 1801, 1801, b'1', '2020-07-15 00:00:00', '7698654332', '65789', NULL, '2020-07-15 00:00:00', 'prueba', 'prueba', '7689546797', NULL, NULL, NULL, NULL, '76542', 'gr@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjgjgjhgjg', 2018, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'ggyuiolpjkfde', 'prueba', 'jhkhjhjk', '223', '4', 'jmhjhjkjh', 'hgjgjghj', '223', '4', 'jhghjgj', 1801, 1801, b'1', '2020-07-15 00:00:00', '5467892354', '76542', NULL, '2020-07-15 00:00:00', 'hggjhjghj', 'hfhgffghf', '7698542387', NULL, NULL, NULL, NULL, '76542', 'kj@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hgjghjg', 2014, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'mjbjhjkhkhjkh', 'kjhjkhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjg', '54', '5', 'hjhgjhghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1991-06-06 00:00:00', 'kjhjkhkj', 'jkhjkh', '6587098754', NULL, NULL, NULL, NULL, '67548', 'q@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhk', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'mjbjhjkhkhjkh', 'kjhjkhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjg', '54', '5', 'hjhgjhghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1991-06-06 00:00:00', 'kjhjkhkj', 'jkhjkh', '6587098754', NULL, NULL, NULL, NULL, '67548', 'q@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhk', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'mjbjhjkhkhjkh', 'kjhjkhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjg', '54', '5', 'hjhgjhghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1991-06-06 00:00:00', 'kjhjkhkj', 'jkhjkh', '6587098754', NULL, NULL, NULL, NULL, '67548', 'q@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhk', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'mjbjhjkhkhjkh', 'kjhjkhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjg', '54', '5', 'hjhgjhghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1991-06-06 00:00:00', 'kjhjkhkj', 'jkhjkh', '6587098754', NULL, NULL, NULL, NULL, '67548', 'q@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhk', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'jgjhjhkhhjkhk', 'kjhkhjkhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'jhghjgjgjg', '54', '5', 'hghgjhghj', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '6666-06-06 00:00:00', 'hjkhkhk', 'jkhjkhk', '564545466', NULL, NULL, NULL, NULL, '45678', 'a@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjhkjh', 6787, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'jgjkhjkhjkhj', 'jkhjkhkjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hgbjhgh', '123', '2', 'hjghjghhjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'hjkhkjh', 'jkhjkh', '768779', NULL, NULL, NULL, NULL, '65789', 'aq@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhjkh', 5677, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'jgjkhjkhjkhj', 'jkhjkhkjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hgbjhgh', '123', '2', 'hjghjghhjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'hjkhkjh', 'jkhjkh', '768779', NULL, NULL, NULL, NULL, '65789', 'aq@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhjkh', 5677, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'jgjkhjkhjkhj', 'jkhjkhkjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hgbjhgh', '123', '2', 'hjghjghhjghjg', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'hjkhkjh', 'jkhjkh', '768779', NULL, NULL, NULL, NULL, '65789', 'aq@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhjkh', 5677, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'iuhkuhjkhjkhj', 'hjjhjkhjkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjgjgjghjgjhg', '45', '4', 'ghfghfh', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'jkhjkhk', 'kjhjkhkjh', '657567576', NULL, NULL, NULL, NULL, '76879', 'ag@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhh', 6578, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'iuhkuhjkhjkhj', 'hjjhjkhjkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjgjgjghjgjhg', '45', '4', 'ghfghfh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'jkhjkhk', 'kjhjkhkjh', '657567576', NULL, NULL, NULL, NULL, '76879', 'ag@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhh', 6578, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'iuhkuhjkhjkhj', 'hjjhjkhjkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjgjgjghjgjhg', '45', '4', 'ghfghfh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'jkhjkhk', 'kjhjkhkjh', '657567576', NULL, NULL, NULL, NULL, '76879', 'ag@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhh', 6578, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'iuhkuhjkhjkhj', 'hjjhjkhjkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjgjgjghjgjhg', '45', '4', 'ghfghfh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'jkhjkhk', 'kjhjkhkjh', '657567576', NULL, NULL, NULL, NULL, '76879', 'ag@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhh', 6578, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'iuhkuhjkhjkhj', 'hjjhjkhjkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjgjgjghjgjhg', '45', '4', 'ghfghfh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'jkhjkhk', 'kjhjkhkjh', '657567576', NULL, NULL, NULL, NULL, '76879', 'ag@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhjkhh', 6578, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'njjkhjhjhjhj', 'hghghgjgh', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjgjhgjg', '656', '6', 'hghjgjhgjhg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '7887-08-08 00:00:00', 'jhjkhkjh', 'jkhjkhjkh', '6575675', NULL, NULL, NULL, NULL, '55678', 'bjhjhj', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjkhjkhk', 67876, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'rfcpruebaqwee', 'peeeeee', 'NULL', 'NULL', 'NULL', 'NULL', 'jhjhkjkhkjhjh', '656', '5', 'hgjgjgj', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-08 00:00:00', 'ggfhf', 'j68', '564565656', NULL, NULL, NULL, NULL, '78979', 'klkl@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hgfgfghg', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'rfcpruebaqwee', 'peeeeee', 'NULL', 'NULL', 'NULL', 'NULL', 'jhjhkjkhkjhjh', '656', '5', 'hgjgjgj', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-08 00:00:00', 'ggfhf', 'j68', '564565656', NULL, NULL, NULL, NULL, '78979', 'klkl@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hgfgfghg', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'gjhhgjghjgjgj', 'jhghjjljiluiuiuyui', 'NULL', 'NULL', 'NULL', 'NULL', 'kjhkkjhjkhkj', '65', '2', 'hghjghjgjh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-07 00:00:00', 'ytuug', 'jgjg', '657657676', NULL, NULL, NULL, NULL, '76567', 'pedro@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'gjhbbj', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'gjhhgjghjgjgj', 'jhghjjljiluiuiuyui', 'NULL', 'NULL', 'NULL', 'NULL', 'kjhkkjhjkhkj', '65', '2', 'hghjghjgjh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-07 00:00:00', 'ytuug', 'jgjg', '657657676', NULL, NULL, NULL, NULL, '76567', 'pedro@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'gjhbbj', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'jgjhghjgjghjg', 'gjhgjgjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hvhghjghghgj', '45', '2', 'hghgjgjhgjh', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-07-07 00:00:00', 'hjgjhghjgh', 'hjghjghjg', '657656757', NULL, NULL, NULL, NULL, '76000', 'hgh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'ghgjghkjhj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'jgjhghjgjghjg', 'gjhgjgjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hvhghjghghgj', '45', '2', 'hghgjgjhgjh', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-07-07 00:00:00', 'hjgjhghjgh', 'hjghjghjg', '657656757', NULL, NULL, NULL, NULL, '76000', 'hgh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'ghgjghkjhj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'jgjhghjgjghjg', 'gjhgjgjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hvhghjghghgj', '45', '2', 'hghgjgjhgjh', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-07-07 00:00:00', 'hjgjhghjgh', 'hjghjghjg', '657656757', NULL, NULL, NULL, NULL, '76000', 'hgh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'ghgjghkjhj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'jgjhghjgjghjg', 'gjhgjgjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hvhghjghghgj', '45', '2', 'hghgjgjhgjh', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-07-07 00:00:00', 'hjgjhghjgh', 'hjghjghjg', '657656757', NULL, NULL, NULL, NULL, '76000', 'hgh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'ghgjghkjhj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'jhghjghjghjgh', 'ghjgjghjgjgjg', 'NULL', 'NULL', 'NULL', 'NULL', 'fghhgjhjkhjk', '5454', 'NULL', 'hghghjghj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-23 00:00:00', 'gjhjghj', 'jhgjghj', '6565768786', NULL, NULL, NULL, NULL, '75687', 'hgh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'gjjhgjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'jhghjghjghjgh', 'ghjgjghjgjgjg', 'NULL', 'NULL', 'NULL', 'NULL', 'fghhgjhjkhjk', '5454', 'NULL', 'hghghjghj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-23 00:00:00', 'gjhjghj', 'jhgjghj', '6565768786', NULL, NULL, NULL, NULL, '75687', 'hgh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'gjjhgjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'rfccccccccccc', 'hkhjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'ghfghfghfhgfh', '243', 'NULL', 'hjghhjgjghj', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-23 00:00:00', 'hfhfghfgh', 'gghffghf', '56465767', NULL, NULL, NULL, NULL, '56578', 'fdd@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'gfghfghgh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'jhjkhjkhkjhjk', 'jkhkjhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hgghghgjgh', '56', 'NULL', 'hgjhjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '6111-02-06 00:00:00', 'jhjkhjkh', 'jkhjhkjh', 'hghgjhghj', NULL, NULL, NULL, NULL, '76000', 'hjg@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhkkjhkj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'jhjkhjkhkjhjk', 'jkhkjhjkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hgghghgjgh', '56', 'NULL', 'hgjhjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '6111-02-06 00:00:00', 'jhjkhjkh', 'jkhjhkjh', 'hghgjhghj', NULL, NULL, NULL, NULL, '76000', 'hjg@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jhkkjhkj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'ijkjkjkljkljk', 'jkljkjkjkljk', 'NULL', 'NULL', 'NULL', 'NULL', 'hvhjghg', '24', '1', 'hggjhgj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'lkjjkl', 'jkkljlk', '657575657', NULL, NULL, NULL, NULL, '76000', 'jhkh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jklklj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'ijkjkjkljkljk', 'jkljkjkjkljk', 'NULL', 'NULL', 'NULL', 'NULL', 'hvhjghg', '24', '1', 'hggjhgj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'lkjjkl', 'jkkljlk', '657575657', NULL, NULL, NULL, NULL, '76000', 'jhkh@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jklklj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'jkhkhkhkjkjhj', 'jhjkkjhkhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghjgjgjh', '45', '4', 'hgjjgjghgj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'gjhjhghjg', '8767867', '65757575', NULL, NULL, NULL, NULL, '76000', 'fgfœgmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjhgj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'jkhkhkhkjkjhj', 'jhjkkjhkhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghjgjgjh', '45', '4', 'hgjjgjghgj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'gjhjhghjg', '8767867', '65757575', NULL, NULL, NULL, NULL, '76000', 'fgfœgmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjhgj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'jkhkhkhkjkjhj', 'jhjkkjhkhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghjgjgjh', '45', '4', 'hgjjgjghgj', 1801, 1801, b'0', NULL, NULL, 'NULL', NULL, '1999-06-06 00:00:00', 'gjhjhghjg', '8767867', '65757575', NULL, NULL, NULL, NULL, '76000', 'fgfœgmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hjhgj', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'bjhjkhkhkhjhk', 'jhkjjkhjkhkjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hkhjhjhjkhkj', '34', '3', 'ljhkjhjkh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhkh', 'kjhjkh', 'jkkhjhk', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jkhh', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'bjhjkhkhkhjhk', 'jhkjjkhjkhkjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hkhjhjhjkhkj', '34', '3', 'ljhkjhjkh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhkh', 'kjhjkh', 'jkkhjhk', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jkhh', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'bjhjkhkhkhjhk', 'jhkjjkhjkhkjh', 'NULL', 'NULL', 'NULL', 'NULL', 'hkhjhjhjkhkj', '34', '3', 'ljhkjhjkh', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhkh', 'kjhjkh', 'jkkhjhk', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'jkhh', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'jjhkhjkhkjhjj', 'jhjkhkjhkhkhk', 'NULL', 'NULL', 'NULL', 'NULL', 'hjghjghjgjh', '23', '3', 'hghghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'hjkhkjh', 'jkhjh', '6575757', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'kjhkjh', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'hgjjjhghjgjhg', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'RFCPRUEBA1234', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'RFCPRUEBA1234', 'jhjkhkjhkh', 'NULL', 'NULL', 'NULL', 'NULL', 'hghghjghjg', '45', '4', 'jgjghjghjg', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-07-23 00:00:00', 'kjhjkhkj', 'kjhjkhh', '56757567', NULL, NULL, NULL, NULL, '76000', 'pmartineze@ceaqueretaro.gob.mx', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'hkjhkjhk', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'MAEP910626CC6', 'sdasdasdsads', 'NULL', 'NULL', 'NULL', 'NULL', 'asdasda', '23', 'NULL', 'sadasdsasa', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1997-07-23 00:00:00', 'sadsda', 'asasds', '2131231232', NULL, NULL, NULL, NULL, '76000', 'pmartineze@ceaqueretaro.gob.mx', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'asdasd', 1994, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'HJH435267777Y', 'prueba', 'NULL', 'NULL', 'NULL', 'NULL', 'conocida', '56', '5', 'centro', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1992-07-26 00:00:00', '76', '6567', '4435675432', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'prueba', 1992, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'HJHSAHSUEY676', 'prueba', 'NULL', 'NULL', 'NULL', 'NULL', 'conocida', '66', '6', 'centro', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1999-06-05 00:00:00', '76', '7686', '4443652787', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'prueba', 1999, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'YUIUIUTG54545', 'prueba', 'NULL', 'NULL', 'NULL', 'NULL', 'conocida', '54', '5', 'centro', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1991-06-06 00:00:00', '76', '565', '4423456786', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'prueba', 1991, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'HJKHJK78678IJ', 'prueba', 'NULL', 'NULL', 'NULL', 'NULL', 'conocida', '122', '2', 'centro', 1801, 1801, b'1', NULL, NULL, 'NULL', NULL, '1992-05-09 00:00:00', '76', '67576', '442356854', NULL, NULL, NULL, NULL, '76000', 'pitsocarron@gmail.com', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'prueba', 1992, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representantes`
--

CREATE TABLE `representantes` (
  `idRepresentantes` int(11) NOT NULL,
  `idPadron` int(11) NOT NULL,
  `NombreRepresentantes` varchar(150) DEFAULT NULL,
  `RFC` varchar(50) DEFAULT NULL,
  `Calle` varchar(150) DEFAULT NULL,
  `NoExterior` varchar(50) DEFAULT NULL,
  `NoInterior` varchar(50) DEFAULT NULL,
  `Colonia` varchar(150) DEFAULT NULL,
  `municipio` int(11) NOT NULL,
  `CP` varchar(50) DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `representantes`
--

INSERT INTO `representantes` (`idRepresentantes`, `idPadron`, `NombreRepresentantes`, `RFC`, `Calle`, `NoExterior`, `NoInterior`, `Colonia`, `municipio`, `CP`, `Correo`, `Telefono`) VALUES
(1, 42, 'jhkhkj', 'jkjkklj', 'kljkljk', '45', '5', 'jgjgjhg', 1801, '67575', 'ygujghjg', '7867878876'),
(2, 44, 'jhkhkj', 'jkjkklj', 'kljkljk', '45', '5', 'jgjgjhg', 1801, '67575', 'ygujghjg', '7867878876'),
(3, 45, 'jhkhkj', 'jkjkklj', 'kljkljk', '45', '5', 'jgjgjhg', 1801, '67575', 'ygujghjg', '7867878876'),
(4, 46, 'jhkhkhkhkhkh', 'cdsfsd', 'hkhkhjk', '34', '2', 'kjhjkhk', 1801, '76000', 'hgjgjg', 'hjgjghjg'),
(5, 47, 'jhkhkhkhkhkh', 'cdsfsd', 'hkhkhjk', '34', '2', 'kjhjkhk', 1801, '76000', 'hgjgjg', 'hjgjghjg'),
(6, 48, 'jhkhkhkhkhkh', 'cdsfsd', 'hkhkhjk', '34', '2', 'kjhjkhk', 1801, '76000', 'hgjgjg', 'hjgjghjg'),
(7, 49, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(8, 50, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(9, 51, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(10, 52, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(11, 53, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(12, 54, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(13, 55, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(14, 56, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(15, 57, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(16, 58, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(17, 59, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(18, 60, 'nbhjg', 'hjgjgjhg', 'jhgjhgjh', '45', '5', 'ghgj', 1801, '76000', 'hghjgj', '657575776'),
(19, 61, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(20, 62, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(21, 63, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(22, 64, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(23, 65, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(24, 66, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(25, 67, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(26, 68, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(27, 69, 'nhnbm', 'HJGHJG', 'JHGHJ', '65', '6', 'BMBNM@gmail.com', 1801, '76000', 'jhk', '1234567890'),
(28, 70, 'pedro', 'XAXXAAXXX', 'CONOCIDO', '123', '1', 'CENTRO', 1801, '76000', 'AR@TEST.COM', ''),
(29, 71, 'prueba', '', '', '', '', '', 0, '', '', ''),
(30, 72, 'prueba', '', '', '', '', '', 0, '', '', ''),
(31, 73, 'prueba', '', '', '', '', '', 0, '', '', ''),
(32, 75, 'pedro', '', '', '', '', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumento`
--

CREATE TABLE `tipodocumento` (
  `idTipoDocumento` int(11) NOT NULL,
  `idPadron` int(11) NOT NULL,
  `NombreDocumento` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `codigo64` text COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tipos de documentos';

--
-- Volcado de datos para la tabla `tipodocumento`
--

INSERT INTO `tipodocumento` (`idTipoDocumento`, `idPadron`, `NombreDocumento`, `codigo64`) VALUES
(1, 73, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(2, 73, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(3, 73, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(4, 73, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(5, 75, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(6, 75, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(7, 75, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh'),
(8, 75, 'docPrueba.txt', 'RXN0byBlcyB1biBkb2N1bWVudG8gZGUgcHJ1ZWJh');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`idMunicipio`);

--
-- Indices de la tabla `padrondesarrolladores`
--
ALTER TABLE `padrondesarrolladores`
  ADD PRIMARY KEY (`idPadron`),
  ADD KEY `idMunicipioFiscal` (`idMunicipioFiscal`);

--
-- Indices de la tabla `representantes`
--
ALTER TABLE `representantes`
  ADD PRIMARY KEY (`idRepresentantes`),
  ADD KEY `idPadron` (`idPadron`),
  ADD KEY `municipio` (`municipio`);

--
-- Indices de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  ADD PRIMARY KEY (`idTipoDocumento`),
  ADD KEY `idPadron` (`idPadron`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `padrondesarrolladores`
--
ALTER TABLE `padrondesarrolladores`
  MODIFY `idPadron` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `representantes`
--
ALTER TABLE `representantes`
  MODIFY `idRepresentantes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  MODIFY `idTipoDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  ADD CONSTRAINT `idPadron` FOREIGN KEY (`idPadron`) REFERENCES `padrondesarrolladores` (`idPadron`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
