<?php
	$enlace = mysqli_connect("localhost", "root", "", "cea");
	error_reporting(0);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<html>  
    <head>  
        <title>Detalle de Desarrolladores</title>  
          
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     </head>     
    <body>  
        <div class="container">
   <br />
   
   <h3 align="center">Detalle de Alta Desarrollador</a></h3><br />
   <br />
   
   <form method="post" id="user_form">
    <div class="table-responsive">
     <table class="table table-striped table-bordered" id="user_data">
      <tr>
       <th>RFC</th>
       <th>Nombre o Razón Social</th>
       <th>Detalle</th>
      </tr>
	  <?php
      $result = mysqli_query($enlace,'SELECT * FROM padrondesarrolladores' );
      while($siglas=mysqli_fetch_array($result)){
    ?>
	<tr>

			<td><?PHP echo $siglas['RFC']; ?></td>
			<td><?PHP echo $siglas['Nombre']; ?></td>
			<td><a href="DetalleDesarrollador.php?RFC=<?PHP echo $siglas['RFC'];?>&username=<?PHP echo $_GET['username'];?>&id=<?php echo $siglas['idPadron']?>" class="btn btn-success">Detalles</a></td>
			<?php
		}
		?>
	</tr>
     </table>
    </div>
   </form>

   <br />
  </div>
  
  <div id="action_alert" title="Action">

  </div>
    </body>  
</html> 
<?php
mysqli_close($enlace);
?>
